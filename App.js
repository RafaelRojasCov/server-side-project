const express = require('express');
const fetch = require('node-fetch');
const cors = require('cors')
const app = express();
const path = require('path');
const bodyParser = require('body-parser');

const PORT = process.env.PORT || 4005;
const DARKSKY_API_KEY = process.env.DARKSKY_API_KEY;
const GOOGLE_API_KEY = process.env.GOOGLE_API_KEY;
const REST_COUNTRIES = process.env.REST_COUNTRIES;
const GOOGLE_GEODATA_API = process.env.GOOGLE_GEODATA_API;
const DARKSKYNET = process.env.DARKSKYNET
const DARKSKY_EXCLUDE = 'exclude=[hourly,daily,flags,alerts]';
const DARKSKY_UNIT = '&units=si';

const Settings = {
  urls: {
    cities: `${REST_COUNTRIES}?fields=capital`,
  },
  buildUrl: {
    geodata: (address) => `${GOOGLE_GEODATA_API}?address=${address}&key=${GOOGLE_API_KEY}`,
    weather: (lat, lng) => `${DARKSKYNET}/${DARKSKY_API_KEY}/${lat},${lng}?${DARKSKY_EXCLUDE}&${DARKSKY_UNIT}`
  }
};

app.use(cors())
app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/capitals/', (req, res) => {
  fetch(Settings.urls.cities, {
        method: 'get',
        headers: { 'X-Requested-With': 'XMLHttpRequest', 'origin': '*' },
    })
    .then(response => {
      if (Math.random(0, 1) < 0.1) {
        throw new Error('How unfortunate! The API Request Failed')
      } else {
        return response.json();
      }
    })
    .then(data => res.send({
      status: 'OK',
      data
    }))
    .catch(error => {
      res.statusCode = 400;
      res.send({
        status: 'NOT-OK',
        message: error.message
      })
    })
});

app.get('/geodata/:address/', (req, res) => {
  fetch(Settings.buildUrl.geodata(req.params.address), {
        method: 'get',
        headers: { 'X-Requested-With': 'XMLHttpRequest', 'origin': '*' },
    })
    .then(response => {
      if (Math.random(0, 1) < 0.1) {
        throw new Error('How unfortunate! The API Request Failed')
      } else {
        return response.json();
      }
    })
    .then(data => {
      if (data && data.code === 400){
        res.send({
          status: 'NOT-OK',
          data: data.error
        })
      } else {
        res.send({
          status: 'OK',
          data
        })
      }
    })
    .catch(error => {
      res.statusCode = 400;
      res.send({
        status: 'NOT-OK',
        message: error.message
      })
    })
})

app.get('/weather/:lat/:lng/', (req, res) => {
  const lat = req.params.lat;
  const lng = req.params.lng;
  fetch(Settings.buildUrl.weather(lat, lng), {
        method: 'get',
        headers: { 'X-Requested-With': 'XMLHttpRequest', 'origin': '*' },
    })
    .then(response => {
      if (Math.random(0, 1) < 0.1) {
        throw new Error('How unfortunate! The API Request Failed')
      } else {
        return response.json();
      }
    })
    .then(data => {
      if (data && data.code === 400){
        res.send({
          status: 'NOT-OK',
          data: data.error
        })
      } else {
        res.send({
          status: 'OK',
          data
        })
      }
    })
    .catch(error => {
      res.statusCode = 400;
      res.send({
        status: 'NOT-OK',
        message: error.message
      })
    })
      
      
})

app.listen(PORT, () => console.log(`The server is running on Port %d`, PORT));